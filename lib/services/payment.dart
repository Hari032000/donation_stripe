import 'dart:convert';

import 'package:stripe_payment/stripe_payment.dart';
import 'package:http/http.dart' as http;

class PaymentResponse {
  String message;
  bool success;
  PaymentResponse({this.message, this.success});
}

class StripeService {
  static String apiBase = 'https://api.stripe.com/v1';
  static String paymentApiUrl = '${StripeService.apiBase}/payment_intents';
  static String secret =
      'sk_test_51IRc10K9OU8igOsQWszFPiKRbzsBT0n5bcnhywFLXFonNMDrN7Da2VghzOIUUdEUFsiBownIEfy4SCVBeLrobTRc00M3mGm2PZ';

  static Map<String, String> headers = {
    'Authorization': 'Bearer ${StripeService.secret}',
    'Content-Type': 'application/x-www-form-urlencoded'
  };

  static init() {
    StripePayment.setOptions(StripeOptions(
        publishableKey:
            "pk_test_51IRc10K9OU8igOsQ0Os1paQyhYMxYmndpUBUdOhunmeGsIv6uF5fgfevOprRWl6hJqbpquKctLfTq9AEHkjCh71Q00g0f8LVpU",
        merchantId: "Test",
        androidPayMode: 'test'));
  }

  static Future<PaymentResponse> payViaExistingCard({
    String amount,
    String currency,
    CreditCard card,
    String description,
    String name,
    String address1,
    String address2,
    String city,
    String state,
    String country,
    String postalCode,
  }) async {
    try {
      var paymentMethod = await StripePayment.createPaymentMethod(
          PaymentMethodRequest(card: card));
      var paymentIntent = await StripeService.createPaymentIntent(
        amount,
        currency,
        description,
        name,
        address1,
        address2,
        city,
        state,
        country,
        postalCode,
      );
      var response = await StripePayment.confirmPaymentIntent(PaymentIntent(
          clientSecret: paymentIntent['client_secret'],
          paymentMethodId: paymentMethod.id));
      if (response.status == 'succeeded') {
        return new PaymentResponse(message: 'Successful', success: true);
      } else {
        return new PaymentResponse(message: 'Failed', success: false);
      }
    } catch (err) {
      return new PaymentResponse(
          message: 'failed : ${err.toString()}', success: true);
    }
  }

  static Future<PaymentResponse> payViaNewCard({
    String amount,
    String currency,
    String description,
    String name,
    String address1,
    String address2,
    String city,
    String state,
    String country,
    String postalCode,
  }) async {
    try {
      var paymentMethod = await StripePayment.paymentRequestWithCardForm(
          CardFormPaymentRequest());
      var paymentIntent = await StripeService.createPaymentIntent(
        amount,
        currency,
        description,
        name,
        address1,
        address2,
        city,
        state,
        country,
        postalCode,
      );
      var response = await StripePayment.confirmPaymentIntent(PaymentIntent(
          clientSecret: paymentIntent['client_secret'],
          paymentMethodId: paymentMethod.id));
      if (response.status == 'succeeded') {
        return new PaymentResponse(message: 'Successful', success: true);
      } else {
        return new PaymentResponse(message: 'Failed', success: false);
      }
    } catch (err) {
      return new PaymentResponse(
          message: 'failed : ${err.toString()}', success: true);
    }
  }

  static Future<Map<String, dynamic>> createPaymentIntent(
    String amount,
    String currency,
    String description,
    String name,
    String address1,
    String address2,
    String city,
    String state,
    String country,
    String postalCode,
  ) async {
    List shipping = [
      {
        'name': 'mark',
        'address': {
          "line1": "510 Townsend St",
          "line2": "510 Townsend St",
          'postal_code': 98140,
          'city': 'San Francisco',
          'state': 'CA',
          'country': 'US',
        },
      },
    ];
    name = shipping[0]['name'];
    address1 = shipping[0]['address']['line1'];
    address2 = shipping[0]['address']['line2'];
    postalCode = shipping[0]['address']['postal_code'];
    city = shipping[0]['address']['city'];
    state = shipping[0]['address']['state'];
    country = shipping[0]['address']['country'];

    try {
      Map<String, dynamic> body = {
        'amount': amount,
        'currency': currency,
        'description': description,
        'name': BillingAddress(name: name),
        "address1": BillingAddress(line1: address1),
        'postal_code': BillingAddress(postalCode: postalCode),
        'city': BillingAddress(city: city),
        'state': BillingAddress(state: state),
        'country': BillingAddress(country: country),
        'payment_method_types[]': 'card'
      };
      var response = await http.post(StripeService.paymentApiUrl,
          body: body, headers: StripeService.headers);
      return jsonDecode(response.body);
    } catch (err) {
      print('err charging user: ${err.toString()}');
    }
    return null;
  }
}
