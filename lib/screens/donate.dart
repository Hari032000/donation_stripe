
import 'package:dpf_profile/services/payment.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:dpf_profile/widget/background.dart';
import 'package:url_launcher/url_launcher.dart';

class Donate extends StatefulWidget {
  @override
  _DonateState createState() => _DonateState();
}

class _DonateState extends State<Donate> {
  bool _value = false;
  String relationship;
  List listitems = ['British', 'Dollor', 'Euro', 'US Dollor'];

  @override
  void initState() {
    super.initState();
    StripeService.init();
  }

  @override
  Widget build(BuildContext context) {
    var _onpressed;
    if (_value) {
      _onpressed = () {
        payVIANewCard(context);
      };
    }
    return Stack(
      children: [
        BackgroundImage(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: Row(
                children: [
                  CircleAvatar(
                    radius: 23,
                    // radius: 2.13 * SizeConfig.heightMultiplier,
                    backgroundColor: Colors.transparent,
                    // backgroundImage: AssetImage('assets/logo.png'),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Text(
                    'Donation',
                    style: TextStyle(color: Colors.white),
                  ),
                  Spacer(
                    flex: 14,
                  )
                ],
              )),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(38.0),
                  child: Text('Thanks for the contribution of Dpf communities',
                      style: TextStyle(color: Colors.white, fontSize: 20)),
                ),
                Column(
                  children: [
                    Container(
                      height: 450,
                      width: 380,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        children: [
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Row(
                                // margin:
                                //     EdgeInsets.only(top: 30, left: 10, right: 150),
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 38.0),
                                    child: Text(
                                      'DONATION',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 150,
                                  ),
                                  DropdownButton(
                                      hint: Text('Currency'),
                                      value: relationship,
                                      onChanged: (newValue) {
                                        setState(() {
                                          relationship = newValue;
                                        });
                                      },
                                      items: listitems.map((valueItem) {
                                        return DropdownMenuItem(
                                            value: valueItem,
                                            child: Text(valueItem));
                                      }).toList()),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Column(
                            children: [
                              TextField(
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold),
                                decoration: InputDecoration(
                                  hintText: '0.00',
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 18.0),
                                child: checkBox(),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              _value == true
                                  ? Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: cards(),
                                    )
                                  : Text(''),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 18.0, left: 55),
                                child: Row(
                                  children: [
                                    // ignore: deprecated_member_use
                                    RaisedButton(
                                        onPressed: _onpressed,
                                        
                                        hoverColor: Colors.greenAccent,
                                        color: Color(0xFFA96469),
                                        disabledColor: Colors.grey,
                                        highlightColor: Colors.green,
                                        child: Text(
                                          'Proceed to Pay',
                                          style: TextStyle(color: Colors.white),
                                        )),

                                    SizedBox(
                                      width: 50,
                                    ),
                                    // ignore: deprecated_member_use
                                    FlatButton(
                                        onPressed: () {},
                                        color: Color(0xFFA96469),
                                        child: Text(
                                          'Cancel',
                                          style: TextStyle(color: Colors.white),
                                        ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget checkBox() {
    return ListTile(
      leading: Checkbox(
        value: _value,
        onChanged: (value) {
          setState(() {
            _value = value;
          });
        },
      ),
      title: RichText(
          text: TextSpan(children: [
        TextSpan(
            text: 'You can Confirm and proceed with payment details.',
            style: TextStyle(color: Colors.grey)),
        TextSpan(
            text: 'for more info',
            style: TextStyle(color: Colors.blue),
            recognizer: TapGestureRecognizer()
              ..onTap = () async {
                var url = "https://www.google.co.in";
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch $url';
                }
              }),
      ])),
    );
  }

  Widget cards() {
    return Padding(
      padding: const EdgeInsets.only(left: 18.0, right: 18.0),
      child: Container(
        child: Column(
          children: [
            TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  prefixIcon: Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: Icon(
                      Icons.credit_card_outlined,
                      size: 30,
                    ),
                  ),
                  hintText: 'Card number'),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 18.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Flexible(
                    child: new TextField(
                      keyboardType: TextInputType.number,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(hintText: 'Expiration date'),
                    ),
                  ),
                  SizedBox(width: 20),
                  new Flexible(
                    child: new TextField(
                        keyboardType: TextInputType.number,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(hintText: 'CVC')),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  payVIANewCard(BuildContext context) async {
    var response = await StripeService.payViaNewCard(
      amount: '1200',
      currency: 'USD',
      description: "Software development services",
      // name: shipping[0]['name'],
      // address: shipping[0]['address']['line1'],
      // postalCode: shipping[0]['address']['postal_code'],
      // city: shipping[0]['address']['city'],
      // state: shipping[0]['address']['state'],
      // country: shipping[0]['address']['country'],
    );

    if (response.success == true) {
      print('res');
      // ignore: deprecated_member_use
      // Scaffold.of(context).showSnackBar(SnackBar(
      //   content: Text(response.message),
      //   duration: new Duration(milliseconds: 3000),
      // new Duration(milliseconds: response.success == true ? 3200 : 3000),
      // )
      // );
    }
    // ignore: deprecated_member_use
  }
}
